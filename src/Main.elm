module Main exposing (..)

import Browser
import Css
import Duration exposing (Duration)
import Energy exposing (Energy)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attributes exposing (css)
import Html.Styled.Events as Events
import Power exposing (Power)
import Quantity exposing (Quantity)


type alias Model =
    { electricityCost : String
    , devicePower : String
    , durationString : String
    , durationUnit : DurationUnit
    , times : String
    , regularMode : RegularMode
    , tab : Tab
    }


type Tab
    = Occasional
    | Regular


type RegularMode
    = Permanent
    | Intermittent


type Msg
    = TypedElectricityCost String
    | TypedDevicePower String
    | TypedDuration String
    | ClickedTab Tab
    | ClickedMode RegularMode
    | TypedTimes String


type alias Flags =
    ()


type DurationUnit
    = Hours
    | Minutes


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { -- Tarifs Tempo 6 kVA août 2023 :
        -- Bleu HP : 0.1369
        -- Bleu HC : 0.1056
        -- Blanc HP : 0.1654
        -- Blanc HC : 0.1246
        -- Rouge HP : 0.7324
        -- Rouge HC : 0.1328
        electricityCost = "0.1369"
      , devicePower = "500"
      , durationString = "1"
      , times = "1"
      , durationUnit = Hours
      , regularMode = Permanent
      , tab = Occasional
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        TypedElectricityCost value ->
            ( { model | electricityCost = value }, Cmd.none )

        TypedDevicePower value ->
            ( { model | devicePower = value }, Cmd.none )

        TypedDuration value ->
            ( { model | durationString = value }, Cmd.none )

        TypedTimes value ->
            ( { model | times = value }, Cmd.none )

        ClickedTab tab ->
            ( { model | tab = tab }, Cmd.none )

        ClickedMode mode ->
            ( { model | regularMode = mode }, Cmd.none )


view : Model -> Html Msg
view model =
    Html.div
        [ css
            [ Css.height (Css.vh 100)
            , Css.display Css.flex_
            , Css.flexDirection Css.column
            , Css.fontFamily Css.sansSerif
            ]
        ]
        [ viewTabs model.tab
        , viewParams model
        , viewResult model
        ]


viewTabs : Tab -> Html Msg
viewTabs currentTab =
    let
        tabStyle : Tab -> Css.Style
        tabStyle tab =
            Css.batch
                [ Css.padding (Css.em 1)
                , Css.textTransform Css.uppercase
                , Css.cursor Css.pointer
                , Css.fontSize (Css.em 0.83)
                , Css.textAlign Css.center
                , Css.backgroundImage Css.none
                , Css.backgroundColor Css.transparent
                , Css.border3 (Css.px 0) Css.solid Css.currentcolor
                , Css.borderBottomWidth
                    (if tab == currentTab then
                        Css.px 5

                     else
                        Css.px 2
                    )
                , Css.hover [ Css.backgroundColor (Css.hsla 60 1 0.6 0.5) ]
                ]
    in
    Html.nav
        [ css
            [ Css.display Css.grid_
            , Css.gridTemplateColumns (Css.trackList (Css.fr 1) [ Css.fr 1 ])
            ]
        ]
        [ Html.button [ Events.onClick (ClickedTab Occasional), css [ tabStyle Occasional ] ] [ Html.text "Ponctuel" ]
        , Html.button [ Events.onClick (ClickedTab Regular), css [ tabStyle Regular ] ] [ Html.text "Régulier" ]
        ]


viewParams : Model -> Html Msg
viewParams model =
    let
        variableFields =
            case model.tab of
                Occasional ->
                    [ Html.label
                        [ Attributes.for "duration"
                        , css
                            [ Css.justifySelf Css.end
                            , Css.textAlign Css.right_
                            ]
                        ]
                        [ Html.text "Durée d’utilisation"
                        , Html.br [] []
                        , Html.span [ css [ Css.fontSize (Css.em 0.8) ] ] [ Html.text "(en heures)" ]
                        ]
                    , Html.div [ css [ Css.justifySelf Css.start ] ]
                        [ Html.input
                            [ Attributes.type_ "number"
                            , Attributes.value model.durationString
                            , Attributes.id "duration"
                            , Events.onInput TypedDuration
                            , css
                                [ Css.width (Css.pct 100)
                                , Css.boxSizing Css.borderBox
                                , Css.padding (Css.em 0.5)
                                ]
                            ]
                            []
                        ]
                    ]

                Regular ->
                    let
                        modeAdditionalStyle : RegularMode -> Css.Style
                        modeAdditionalStyle mode =
                            if mode == model.regularMode then
                                Css.batch
                                    [ Css.backgroundColor (Css.hex "#333")
                                    , Css.color (Css.hex "#fff")
                                    ]

                            else
                                Css.batch []
                    in
                    [ Html.button
                        [ Events.onClick (ClickedMode Permanent)
                        , css <|
                            [ Css.padding (Css.em 1)
                            , Css.cursor Css.pointer
                            , Css.fontSize (Css.em 0.83)
                            , Css.backgroundImage Css.none
                            , Css.backgroundColor Css.transparent
                            , Css.border3 (Css.px 1) Css.solid Css.currentcolor
                            , modeAdditionalStyle Permanent
                            ]
                        ]
                        [ Html.text "Utilisation permanente" ]
                    , Html.button
                        [ Events.onClick (ClickedMode Intermittent)
                        , css <|
                            [ Css.padding (Css.em 1)
                            , Css.cursor Css.pointer
                            , Css.fontSize (Css.em 0.83)
                            , Css.backgroundImage Css.none
                            , Css.backgroundColor Css.transparent
                            , Css.border3 (Css.px 1) Css.solid Css.currentcolor
                            , modeAdditionalStyle Intermittent
                            ]
                        ]
                        [ Html.text "Utilisation intermittente" ]
                    ]
                        ++ (if model.regularMode == Intermittent then
                                [ Html.label
                                    [ Attributes.for "duration"
                                    , css
                                        [ Css.justifySelf Css.end
                                        , Css.textAlign Css.right_
                                        ]
                                    ]
                                    [ Html.text "Durée d’utilisation"
                                    , Html.br [] []
                                    , Html.span [ css [ Css.fontSize (Css.em 0.8) ] ] [ Html.text "(en heures)" ]
                                    ]
                                , Html.div [ css [ Css.justifySelf Css.start ] ]
                                    [ Html.input
                                        [ Attributes.type_ "number"
                                        , Attributes.value model.durationString
                                        , Attributes.id "duration"
                                        , Events.onInput TypedDuration
                                        , css
                                            [ Css.width (Css.pct 100)
                                            , Css.boxSizing Css.borderBox
                                            , Css.padding (Css.em 0.5)
                                            ]
                                        ]
                                        []
                                    ]
                                , Html.label [ css [ Css.textAlign Css.end ] ]
                                    [ Html.input
                                        [ Attributes.type_ "number"
                                        , Attributes.size 5
                                        , Events.onInput TypedTimes
                                        , Attributes.value model.times
                                        ]
                                        []
                                    , Html.text " fois"
                                    ]
                                , Html.text "par mois"
                                ]

                            else
                                []
                           )
    in
    Html.div
        [ css
            [ Css.display Css.grid_
            , Css.placeItems Css.center
            , Css.flexGrow (Css.num 1)
            ]
        ]
        [ Html.div
            [ css
                [ Css.padding (Css.em 0.5)
                , Css.display Css.grid_
                , Css.gap2 (Css.em 2) (Css.em 1)
                , Css.gridTemplateColumns (Css.trackList (Css.fr 1) [ Css.fr 1 ])
                , Css.alignItems Css.center
                ]
            ]
            ([ Html.label
                [ Attributes.for "electricityCost"
                , css
                    [ Css.justifySelf Css.end
                    , Css.textAlign Css.right_
                    ]
                ]
                [ Html.text "Coût du kWh "
                , Html.br [] []
                , Html.span [ css [ Css.fontSize (Css.em 0.8) ] ]
                    [ Html.text "(en €)" ]
                ]
             , Html.div [ css [ Css.justifySelf Css.start ] ]
                [ Html.input
                    [ Attributes.type_ "number"
                    , Attributes.value model.electricityCost
                    , Attributes.id "electricityCost"
                    , Events.onInput TypedElectricityCost
                    , css
                        [ Css.width (Css.pct 100)
                        , Css.boxSizing Css.borderBox
                        , Css.padding (Css.em 0.5)
                        ]
                    ]
                    []
                ]
             , Html.label
                [ Attributes.for "devicePower"
                , css
                    [ Css.justifySelf Css.end
                    , Css.textAlign Css.right_
                    ]
                ]
                [ Html.text "Puissance de l’appareil"
                , Html.br [] []
                , Html.span [ css [ Css.fontSize (Css.em 0.8) ] ] [ Html.text "(en kWh)" ]
                ]
             , Html.div
                [ css [ Css.justifySelf Css.start ] ]
                [ Html.input
                    [ Attributes.type_ "number"
                    , Attributes.value model.devicePower
                    , Events.onInput TypedDevicePower
                    , Attributes.id "devicePower"
                    , css
                        [ Css.width (Css.pct 100)
                        , Css.boxSizing Css.borderBox
                        , Css.padding (Css.em 0.5)
                        ]
                    ]
                    []
                ]
             ]
                ++ variableFields
            )
        ]


viewResult : Model -> Html Msg
viewResult model =
    let
        ( textBefore, textAfter ) =
            case model.tab of
                Occasional ->
                    ( "Une utilisation ponctuelle", "" )

                Regular ->
                    ( "L’utilisation régulière", " par mois" )
    in
    Html.div
        [ css
            [ Css.flexGrow (Css.num 1)
            , Css.display Css.flex_
            , Css.justifyContent Css.center
            , Css.alignItems Css.center
            ]
        ]
        [ Html.span [ css [ Css.textAlign Css.center, Css.padding (Css.em 0.5) ] ]
            [ Html.text <| textBefore ++ " de cet appareil vous coûte "
            , Html.strong []
                [ Html.text (computeCost model |> viewCost)
                ]
            , Html.text textAfter
            ]
        ]


computeCost : Model -> Maybe Float
computeCost model =
    case model.tab of
        Occasional ->
            computeCostOccasional model

        Regular ->
            computeCostRegular model


computeCostOccasional : Model -> Maybe Float
computeCostOccasional model =
    case
        ( String.toFloat model.electricityCost
        , String.toFloat model.devicePower
        , String.toFloat model.durationString
        )
    of
        ( Just cost, Just watts, Just hours ) ->
            let
                duration : Duration
                duration =
                    case model.durationUnit of
                        Hours ->
                            Duration.hours hours

                        Minutes ->
                            Duration.minutes hours
            in
            Just <| computeCostCore cost watts duration

        _ ->
            Nothing


computeCostRegular : Model -> Maybe Float
computeCostRegular model =
    let
        maybeCost : Maybe Float
        maybeCost =
            String.toFloat model.electricityCost

        maybeWatts : Maybe Float
        maybeWatts =
            String.toFloat model.devicePower

        maybeDurationInHours : Maybe Float
        maybeDurationInHours =
            String.toFloat model.durationString

        maybeTimes : Maybe Float
        maybeTimes =
            String.toFloat model.times
    in
    case ( maybeCost, maybeWatts ) of
        ( Just cost, Just watts ) ->
            case model.regularMode of
                Permanent ->
                    Just <| computeCostCore cost watts (Duration.hours <| 30 * 24)

                Intermittent ->
                    case ( maybeDurationInHours, maybeTimes ) of
                        ( Just durationInHours, Just times ) ->
                            Just <| computeCostCore cost watts (Duration.hours <| durationInHours * times)

                        _ ->
                            Nothing

        _ ->
            Nothing


computeCostCore : Float -> Float -> Duration -> Float
computeCostCore cost watts duration =
    let
        power : Power
        power =
            Power.watts watts

        energy : Energy
        energy =
            power |> Quantity.for duration
    in
    cost * (energy |> Energy.inKilowattHours)


viewCost : Maybe Float -> String
viewCost maybeCost =
    case maybeCost of
        Just cost ->
            String.replace "." "," (String.fromFloat cost)
                ++ " €"

        Nothing ->
            "(valeur invalide)"


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view >> Html.toUnstyled
        , subscriptions = subscriptions
        }
